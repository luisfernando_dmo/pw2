package interfaces;

import java.io.Serializable;
import java.util.List;

public interface GenericDAOInterface<T, id extends Serializable> {
    public boolean persist(T entity);

    public boolean update(T entity);

    public T findById(Integer id);

    public boolean delete(T entity);

    public List<T> findAll();
}