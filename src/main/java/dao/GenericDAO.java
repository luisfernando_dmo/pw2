package dao;

import interfaces.GenericDAOInterface;
import util.HibernateUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import java.util.List;

public class GenericDAO<T> implements GenericDAOInterface<T, Integer> {
    private EntityTransaction transaction;
    private EntityManager entityManager;

    private final Class<T> type;

    public GenericDAO(Class<T> type){
        this.type = type;
        this.entityManager = HibernateUtil.getEntityManager();
        this.transaction = entityManager.getTransaction();
    }

    @Override
    public boolean persist(T entity) {
        try{
            transaction.begin();
                entityManager.persist(entity);
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public boolean update(T entity) {
        try{
            transaction.begin();
                entityManager.merge(entity);
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public T findById(Integer id) {
        return entityManager.find(type, id);
    }

    @Override
    public boolean delete(T entity) {
        try{
            transaction.begin();
                entityManager.remove(entityManager.contains(entity) ? entity : entityManager.merge(entity));
            transaction.commit();
            return true;
        }catch (PersistenceException e){
            transaction.rollback();
            e.printStackTrace();
            return false;
        }finally {
            entityManager.close();
        }
    }

    @Override
    public List<T> findAll() {
        return entityManager.createQuery("FROM " + type.getName()).getResultList();
    }
}
