package util;

import java.awt.*;

public class ConstantUtil {
    public static String[] OPTIONS = {"Departament", "Dependent", "Employee", "Project"};

    public static Color GREEN_APP_COLOR = new Color(92, 184, 92);
    public static Color BLUE_APP_COLOR = new Color(66, 139, 202);

    public static Color RED_APP_COLOR = new Color(220,20,60);
}