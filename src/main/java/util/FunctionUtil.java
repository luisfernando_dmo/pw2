package util;

import javax.swing.*;
import java.awt.*;

public class FunctionUtil {
    public static void alert(String message){
        JWindow jWindow = new JWindow();

        JPanel jPanel = new JPanel();
        jPanel.setLayout(new GridLayout(2, 1));

        JLabel jLabel = new JLabel(message, SwingConstants.CENTER);

        jPanel.add(jLabel);

        jWindow.getContentPane().add(jPanel);
        jWindow.setSize(new Dimension(350, 70));
        jWindow.setMinimumSize(new Dimension(350, 70));

        jWindow.setLocationRelativeTo(null);

        try {
            // 1.6+
            jWindow.setLocationByPlatform(true);
            jWindow.setMinimumSize(jWindow.getSize());
        } catch(Throwable ignoreAndContinue) {
            ignoreAndContinue.printStackTrace();
        }

        jWindow.setVisible(true);
    }
}
