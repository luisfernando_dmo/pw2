package util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class HibernateUtil {
    private static HibernateUtil instance = null;

    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    private HibernateUtil(){
       entityManagerFactory = Persistence.createEntityManagerFactory("pw2");
    }

    public static HibernateUtil getInstance(){
        if(instance == null){
            instance  = new HibernateUtil();
        }
        return instance;
    }

    public static EntityManagerFactory getSessionFactory() {
        return entityManagerFactory;
    }

    public static EntityManager getEntityManager() {
        return entityManagerFactory.createEntityManager();
    }
}
