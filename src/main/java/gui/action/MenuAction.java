package gui.action;

import gui.select.CreateSelect;
import gui.select.RudSelect;

import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

public class MenuAction {
    private String message;

    public List<ActionListener> getActions(){

        /* List of Actions */
        List<ActionListener> actionListeners = new ArrayList<>();

        /* Action button "Create" */
        actionListeners.add(e -> {
            new CreateSelect().showOptions();
        });

        /* Action button "RUD" */
        actionListeners.add(e -> {
            new RudSelect().showOptions();
        });

        return actionListeners;
    }
}