package gui.form;

import dao.GenericDAO;
import entity.Department;
import javafx.event.ActionEvent;
import util.ConstantUtil;
import util.FunctionUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class DepartmentForm {
    public void show(){
        JFrame frame = new JFrame("Departament - Create");
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new GridLayout(10, 1));

        JLabel lblName = new JLabel("Name (*)");
        jPanel.add(lblName);

        JTextField textField = new JTextField();
        jPanel.add(textField);
        textField.setColumns(10);

        JLabel lblNumber = new JLabel("Number (*)");
        jPanel.add(lblNumber);

        JTextField textField_1 = new JTextField();
        textField_1.setColumns(10);
        jPanel.add(textField_1);

        JTextField jTextFieldBlank = new JTextField();
        jTextFieldBlank.setVisible(false);

        jPanel.add(jTextFieldBlank);

        JButton btnSubmit = new JButton("Submit");
        btnSubmit.setBackground(ConstantUtil.BLUE_APP_COLOR);
        btnSubmit.setForeground(ConstantUtil.GREEN_APP_COLOR);
        btnSubmit.setSize(100, 100);
        jPanel.add(btnSubmit);

        JButton btnClear = new JButton("Clear");
        jPanel.add(btnClear);

        jPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Required fields (*)"));

        frame.getContentPane().add(jPanel);

        btnSubmit.addActionListener(e -> {
            Department department = new Department();
            department.setName(textField.getText());
            department.setNumber(Integer.parseInt(textField_1.getText()));

            GenericDAO<Department> departmentGenericDAO = new GenericDAO<>(Department.class);

            if(departmentGenericDAO.persist(department)){
                textField_1.setText(null);
                textField.setText(null);
                FunctionUtil.alert("Data successfully registered!");
            }else{
                FunctionUtil.alert("Error registering data!");
            }

        });

        btnClear.addActionListener(e -> {
            textField_1.setText(null);
            textField.setText(null);
        });

        frame.setSize(new Dimension(400, 400));
        frame.setResizable(false);
        frame.setMinimumSize(new Dimension(400, 400));
        frame.pack();

        frame.setLocationRelativeTo(null);

        try {
            // 1.6+
            frame.setLocationByPlatform(true);
            frame.setMinimumSize(frame.getSize());
        } catch(Throwable ignoreAndContinue) {
            ignoreAndContinue.printStackTrace();
        }

        frame.setVisible(true);
    }
}
