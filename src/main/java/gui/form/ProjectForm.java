package gui.form;

import com.google.gson.Gson;
import dao.GenericDAO;
import entity.Department;
import entity.Employee;
import entity.Project;
import org.json.JSONException;
import org.json.JSONObject;
import util.ConstantUtil;
import util.FunctionUtil;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class ProjectForm {

    private List<Employee> employees;
    private List<Department> departments;

    public ProjectForm(){
        this.employees = new GenericDAO<Employee>(Employee.class)
                .findAll();

        this.departments = new GenericDAO<Department>(Department.class)
                .findAll();
    }

    public void show(){
        JButton btnSubmit = new JButton("Submit");
        JButton btnClear = new JButton("Clear");

        JComboBox<String> comboBoxEmployee;
        JComboBox<String> comboBoxDepartament;

        JFrame frame = new JFrame("Project - Create");
        JPanel jPanel = new JPanel();

        jPanel.setLayout(new GridLayout(10, 1));

        JLabel lblOccupation = new JLabel("Research Employee (*)");
        jPanel.add(lblOccupation);

        comboBoxEmployee = new JComboBox<String>();
        comboBoxEmployee.addItem("Select an option");
        for(Employee employee: employees){
            String json = new Gson().toJson(employee.getData());
            JSONObject object = new JSONObject(json);
            try {
                if(employee.getRole().getId() == 1){
                    comboBoxEmployee.addItem(object.getString("name"));
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        jPanel.add(comboBoxEmployee);

        JLabel lblDepartment = new JLabel("Department (*)");
        jPanel.add(lblDepartment);

        comboBoxDepartament= new JComboBox<String>();
        comboBoxDepartament.addItem("Select an option");
        for(Department department: departments){
            comboBoxDepartament.addItem(department.getName());
        }

        jPanel.add(comboBoxDepartament);

        JLabel lblName;
        JTextField textFieldName;

        lblName = new JLabel("Name (*)");
        jPanel.add(lblName);

        textFieldName = new JTextField();
        textFieldName.setColumns(10);
        jPanel.add(textFieldName);

        JLabel lblHour;
        JTextField textFieldHour;

        lblHour = new JLabel("Hour (*)");
        jPanel.add(lblHour);

        textFieldHour = new JTextField();
        textFieldHour.setColumns(10);
        jPanel.add(textFieldHour);

        JLabel lblDate;
        JTextField textFieldDate;

        lblDate = new JLabel("Date (*)");
        jPanel.add(lblDate);

        textFieldDate = new JTextField();
        textFieldDate.setColumns(10);
        jPanel.add(textFieldDate);

        btnClear = new JButton("Clear");
        jPanel.add(btnClear);

        btnClear.addActionListener(e -> {
            comboBoxEmployee.setSelectedIndex(0);
            comboBoxDepartament.setSelectedIndex(0);
            textFieldName.setText(null);
            textFieldHour.setText(null);
            textFieldDate.setText(null);
        });

        btnSubmit = new JButton("Submit");
        btnSubmit.setBackground(ConstantUtil.BLUE_APP_COLOR);
        btnSubmit.setForeground(ConstantUtil.GREEN_APP_COLOR);
        btnSubmit.setSize(100, 100);
        jPanel.add(btnSubmit);

        btnSubmit.addActionListener(e -> {
            Project project = new Project();
            project.setEmployee(employees.get(comboBoxEmployee.getSelectedIndex()-1));
            if(comboBoxDepartament.getSelectedIndex() > 0){
                project.setDepartment(departments.get(comboBoxDepartament.getSelectedIndex()-1));
            }

            project.setDate(textFieldDate.getText().toString());
            project.setHour(textFieldHour.getText().toString());
            project.setName(textFieldName.getText().toString());

            GenericDAO<Project> projectGenericDAO = new GenericDAO<>(Project.class);

            if(projectGenericDAO.persist(project)){
                comboBoxEmployee.setSelectedIndex(0);
                comboBoxDepartament.setSelectedIndex(0);
                textFieldName.setText(null);
                textFieldHour.setText(null);
                textFieldDate.setText(null);
                FunctionUtil.alert("Data successfully registered!");
            }else{
                FunctionUtil.alert("<html>Verify that the project name is already in use.</html>"
                );
            }

        });

        jPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Required fields (*)"));

        frame.getContentPane().add(jPanel);

        frame.setSize(new Dimension(400, 400));
        frame.setResizable(false);
        frame.setMinimumSize(new Dimension(400, 400));
        frame.pack();

        frame.setLocationRelativeTo(null);

        try {
            // 1.6+
            frame.setLocationByPlatform(true);
            frame.setMinimumSize(frame.getSize());
        } catch(Throwable ignoreAndContinue) {
            ignoreAndContinue.printStackTrace();
        }

        frame.setVisible(true);
    }
}