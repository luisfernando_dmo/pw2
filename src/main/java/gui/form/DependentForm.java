package gui.form;


import com.google.gson.Gson;
import dao.GenericDAO;
import entity.Dependent;
import entity.Employee;
import entity.Role;
import org.json.JSONException;
import org.json.JSONObject;
import util.ConstantUtil;
import util.FunctionUtil;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.Objects;

public class DependentForm {

    private List<Employee> employees;

    public DependentForm(){
        this.employees = new GenericDAO<Employee>(Employee.class)
                .findAll();
    }

    public void show(){
        JButton btnSubmit = new JButton("Submit");
        JButton btnClear = new JButton("Clear");

        JComboBox<String> comboBoxEmployee;

        JFrame frame = new JFrame("Dependent - Create");
        JPanel jPanel = new JPanel();

        jPanel.setLayout(new GridLayout(10, 1));

        JLabel lblOccupation = new JLabel("Employee (*)");
        jPanel.add(lblOccupation);

        comboBoxEmployee = new JComboBox<String>();
        comboBoxEmployee.addItem("Select an option");
        for(Employee employee: employees){
            String json = new Gson().toJson(employee.getData());
            JSONObject object = new JSONObject(json);
            try {
                comboBoxEmployee.addItem(object.getString("name"));
            }catch (JSONException e){
                e.printStackTrace();
            }
        }

        jPanel.add(comboBoxEmployee);

        JLabel lblName;
        JTextField textFieldName;

        JLabel lblSex;
        JComboBox<String> comboBoxSex;

        JLabel lblBirthday;
        JTextField textFieldBirthday;

        JLabel lblKinship;
        JTextField textFieldKinship;

        lblName = new JLabel("Name (*)");
        jPanel.add(lblName);

        textFieldName = new JTextField();
        textFieldName.setColumns(10);
        jPanel.add(textFieldName);

        lblSex = new JLabel("Sex (*)");
        jPanel.add(lblSex);

        comboBoxSex = new JComboBox<String>();
        comboBoxSex.addItem("Select an option");
        comboBoxSex.addItem("Male");
        comboBoxSex.addItem("Female");
        jPanel.add(comboBoxSex);

        lblBirthday = new JLabel("Birthday (*)");
        jPanel.add(lblBirthday);

        textFieldBirthday = new JTextField();
        textFieldBirthday.setColumns(10);
        jPanel.add(textFieldBirthday);

        lblKinship = new JLabel("Kinship (*) ");
        jPanel.add(lblKinship);

        textFieldKinship = new JTextField();
        textFieldKinship.setColumns(10);
        jPanel.add(textFieldKinship);

        jPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Required fields (*)"));

        btnClear = new JButton("Clear");
        jPanel.add(btnClear);

        btnClear.addActionListener(e -> {
            textFieldName.setText(null);
            comboBoxEmployee.setSelectedIndex(0);
            comboBoxSex.setSelectedIndex(0);
            textFieldBirthday.setText(null);
            textFieldKinship.setText(null);
        });

        btnSubmit = new JButton("Submit");
        btnSubmit.setBackground(ConstantUtil.BLUE_APP_COLOR);
        btnSubmit.setForeground(ConstantUtil.GREEN_APP_COLOR);
        btnSubmit.setSize(100, 100);
        jPanel.add(btnSubmit);

        btnSubmit.addActionListener(e -> {
            Dependent dependent = new Dependent();
            dependent.setEmployee(employees.get(comboBoxEmployee.getSelectedIndex()-1));
            dependent.setName(textFieldName.getText());
            dependent.setSex(Objects.requireNonNull(comboBoxSex.getSelectedItem()).toString());
            dependent.setBirthday(textFieldBirthday.getText());
            dependent.setKinship(textFieldKinship.getText());

            GenericDAO<Dependent> dependentGenericDAO = new GenericDAO<>(Dependent.class);

            if(dependentGenericDAO.persist(dependent)){
                textFieldName.setText(null);
                comboBoxEmployee.setSelectedIndex(0);
                comboBoxSex.setSelectedIndex(0);
                textFieldBirthday.setText(null);
                textFieldKinship.setText(null);
                FunctionUtil.alert("Data successfully registered!");
            }else{
                FunctionUtil.alert("Error registering data!");
            }
        });

        frame.getContentPane().add(jPanel);

        frame.setSize(new Dimension(400, 400));
        frame.setResizable(false);
        frame.setMinimumSize(new Dimension(400, 400));
        frame.pack();

        frame.setLocationRelativeTo(null);

        try {
            // 1.6+
            frame.setLocationByPlatform(true);
            frame.setMinimumSize(frame.getSize());
        } catch(Throwable ignoreAndContinue) {
            ignoreAndContinue.printStackTrace();
        }

        frame.setVisible(true);
    }
}