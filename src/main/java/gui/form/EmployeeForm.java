package gui.form;

import dao.GenericDAO;
import entity.Department;
import entity.Employee;
import entity.Role;
import javafx.event.ActionEvent;
import model.CleaningAssistant;
import model.Researcher;
import model.Secretary;
import util.ConstantUtil;
import util.FunctionUtil;
import util.HibernateUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceException;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;

public class EmployeeForm {

    private List<Role> roles;
    private List<Department> departments;

    private JButton btnSubmit = new JButton("Submit");
    private JButton btnClear = new JButton("Clear");

    private JComboBox<String> comboBoxTypeEmployee;
    private JComboBox<String> comboBoxDepartment;

    private JFrame frame = new JFrame("Employee - Create");
    private JPanel jPanel = new JPanel();

    public EmployeeForm(){
        this.roles = new GenericDAO<Role>(Role.class).findAll();
        this.departments = new GenericDAO<Department>(Department.class).findAll();
    }

    public void show(){
        jPanel.setLayout(new GridLayout(12, 1));

        JLabel lblOccupation = new JLabel("Type employee (*)");
        jPanel.add(lblOccupation);

        comboBoxTypeEmployee = new JComboBox<String>();
        comboBoxTypeEmployee.addItem("Select an option");
        for(Role role: roles){
            comboBoxTypeEmployee.addItem(role.getName());
        }
        jPanel.add(comboBoxTypeEmployee);

        JLabel lblDepartment = new JLabel("Department (*)");
        jPanel.add(lblDepartment);

        comboBoxDepartment = new JComboBox<String>();
        comboBoxDepartment.addItem("Select an option");
        for(Department department: departments){
            comboBoxDepartment.addItem(department.getName());
        }
        jPanel.add(comboBoxDepartment);

        comboBoxTypeEmployee.addActionListener(e -> {
            if(!Objects.requireNonNull(comboBoxTypeEmployee.getSelectedItem()).equals("Select an option")){
                switch (roles.get(comboBoxTypeEmployee.getSelectedIndex()-1).getName()){
                    case "Pesquisador":
                        if(textFieldName != null){
                            if(textFieldJourney != null){
                                hiddenFieldsCleaningAssistant();
                            }
                            hiddenFieldsResearcher();
                        }
                        showFieldsResearcher();
                        break;

                    case "Secretário":
                        if(textFieldName != null){
                            if(textFieldJourney != null){
                                hiddenFieldsCleaningAssistant();
                            }
                            hiddenFieldsResearcher();
                        }
                        showFieldsSecretary();
                        break;

                    case "Auxiliar de Limpeza":
                        if(textFieldName != null){
                            if(textFieldJourney != null){
                                hiddenFieldsCleaningAssistant();
                            }
                            hiddenFieldsResearcher();
                        }
                        showFieldsCleaningAssistant();
                        break;
                }
            }else{
                if(textFieldJourney != null){
                    hiddenFieldsCleaningAssistant();
                }
                hiddenFieldsResearcher();
            }
        });

        jPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Required fields (*)"));

        frame.getContentPane().add(jPanel);

        frame.setSize(new Dimension(400, 400));
        frame.setResizable(false);
        frame.setMinimumSize(new Dimension(400, 400));
        frame.pack();

        frame.setLocationRelativeTo(null);

        try {
            // 1.6+
            frame.setLocationByPlatform(true);
            frame.setMinimumSize(frame.getSize());
        } catch(Throwable ignoreAndContinue) {
            ignoreAndContinue.printStackTrace();
        }

        frame.setVisible(true);
    }

    private JLabel lblName;
    private JTextField textFieldName;

    private JLabel lblAddress;
    private JTextField textFieldAddress;

    private JLabel lblSex;
    private JComboBox<String> comboBoxSex;

    private JLabel lblBirthday;
    private JTextField textFieldBirthday;

    private JLabel lblSalary;
    private JTextField textFieldSalary;

    private JLabel lblActing;
    private JTextField textFieldActing;

    private void showFieldsResearcher(){
        lblName = new JLabel("Name (*)");
        jPanel.add(lblName);

        textFieldName = new JTextField();
        textFieldName.setColumns(10);
        jPanel.add(textFieldName);

        lblAddress = new JLabel("Address (*) ");
        jPanel.add(lblAddress);

        textFieldAddress = new JTextField();
        textFieldAddress.setColumns(10);
        jPanel.add(textFieldAddress);

        lblSex = new JLabel("Sex (*)");
        jPanel.add(lblSex);

        comboBoxSex = new JComboBox<String>();
        comboBoxSex.addItem("Select an option");
        comboBoxSex.addItem("Male");
        comboBoxSex.addItem("Female");
        jPanel.add(comboBoxSex);

        lblBirthday = new JLabel("Birthday (*)");
        jPanel.add(lblBirthday);

        textFieldBirthday = new JTextField();
        textFieldBirthday.setColumns(10);
        jPanel.add(textFieldBirthday);

        lblSalary = new JLabel("Salary (*) {1.0}");
        jPanel.add(lblSalary);

        textFieldSalary = new JTextField();
        textFieldSalary.setColumns(10);
        jPanel.add(textFieldSalary);

        lblActing = new JLabel("Acting (*)");
        jPanel.add(lblActing);

        textFieldActing = new JTextField();
        textFieldActing.setColumns(10);
        jPanel.add(textFieldActing);

        btnClear = new JButton("Clear");
        jPanel.add(btnClear);

        btnSubmit = new JButton("Submit");
        btnSubmit.setBackground(ConstantUtil.BLUE_APP_COLOR);
        btnSubmit.setForeground(ConstantUtil.GREEN_APP_COLOR);
        btnSubmit.setSize(100, 100);
        jPanel.add(btnSubmit);

        btnSubmit.addActionListener(e -> {
            Researcher researcher = new Researcher(
                    textFieldName.getText(),
                    textFieldAddress.getText(),
                    Objects.requireNonNull(comboBoxSex.getSelectedItem()).toString(),
                    textFieldBirthday.getText(),
                    Double.parseDouble(textFieldSalary.getText()),
                    textFieldActing.getText()
            );

            Employee<Researcher> employee = new Employee<>();
            employee.setRole(roles.get(comboBoxTypeEmployee.getSelectedIndex()-1));
            employee.setDepartment(departments.get(comboBoxDepartment.getSelectedIndex()-1));
            employee.setData(researcher);

            GenericDAO<Employee> genericDAO = new GenericDAO<>(Employee.class);

            if(genericDAO.persist(employee)){
                clear();
                FunctionUtil.alert("Data successfully registered!");
            }else{
                FunctionUtil.alert("Error registering data!");
            }
        });

        btnClear.addActionListener(e -> {
            clear();
        });

        frame.getContentPane().add(jPanel);
        frame.setVisible(true);
    }

    private void clear(){
        textFieldName.setText(null);
        textFieldAddress.setText(null);
        comboBoxSex.setSelectedIndex(0);
        textFieldBirthday.setText(null);
        textFieldSalary.setText(null);
        textFieldActing.setText(null);
        comboBoxDepartment.setSelectedIndex(0);
        if(textFieldJourney != null){
            textFieldJourney.setText(null);
            textFieldDescription.setText(null);
        }
    }

    private void hiddenButtonsActions(){
        jPanel.remove(btnSubmit);
        jPanel.remove(btnClear);
    }

    private void hiddenFieldsResearcher(){
        jPanel.remove(lblName);
        jPanel.remove(textFieldName);

        jPanel.remove(lblAddress);
        jPanel.remove(textFieldAddress);

        jPanel.remove(lblSex);
        jPanel.remove(comboBoxSex);

        jPanel.remove(lblBirthday);
        jPanel.remove(textFieldBirthday);

        jPanel.remove(lblSalary);
        jPanel.remove(textFieldSalary);

        jPanel.remove(lblActing);
        jPanel.remove(textFieldActing);

        hiddenButtonsActions();

        frame.revalidate();
        frame.repaint();
    }

    private void showFieldsSecretary(){
        lblName = new JLabel("Name (*)");
        jPanel.add(lblName);

        textFieldName = new JTextField();
        textFieldName.setColumns(10);
        jPanel.add(textFieldName);

        lblAddress = new JLabel("Address (*) ");
        jPanel.add(lblAddress);

        textFieldAddress = new JTextField();
        textFieldAddress.setColumns(10);
        jPanel.add(textFieldAddress);

        lblSex = new JLabel("Sex (*)");
        jPanel.add(lblSex);

        comboBoxSex = new JComboBox<String>();
        comboBoxSex.addItem("Select an option");
        comboBoxSex.addItem("Male");
        comboBoxSex.addItem("Female");
        jPanel.add(comboBoxSex);

        lblBirthday = new JLabel("Birthday (*)");
        jPanel.add(lblBirthday);

        textFieldBirthday = new JTextField();
        textFieldBirthday.setColumns(10);
        jPanel.add(textFieldBirthday);

        lblSalary = new JLabel("Salary (*) {1.0}");
        jPanel.add(lblSalary);

        textFieldSalary = new JTextField();
        textFieldSalary.setColumns(10);
        jPanel.add(textFieldSalary);

        lblActing = new JLabel("Schooling (*)");
        jPanel.add(lblActing);

        textFieldActing = new JTextField();
        textFieldActing.setColumns(10);
        jPanel.add(textFieldActing);

        btnClear = new JButton("Clear");
        jPanel.add(btnClear);

        btnSubmit = new JButton("Submit");
        btnSubmit.setBackground(ConstantUtil.BLUE_APP_COLOR);
        btnSubmit.setForeground(ConstantUtil.GREEN_APP_COLOR);
        btnSubmit.setSize(100, 100);
        jPanel.add(btnSubmit);

        btnSubmit.addActionListener(e -> {
            Secretary secretary = new Secretary(
                    textFieldName.getText(),
                    textFieldAddress.getText(),
                    Objects.requireNonNull(comboBoxSex.getSelectedItem()).toString(),
                    textFieldBirthday.getText(),
                    Double.parseDouble(textFieldSalary.getText()),
                    textFieldActing.getText()
            );

            Employee<Secretary> employee = new Employee<>();
            employee.setRole(roles.get(comboBoxTypeEmployee.getSelectedIndex()-1));
            employee.setDepartment(departments.get(comboBoxDepartment.getSelectedIndex()-1));
            employee.setData(secretary);

            GenericDAO<Employee> genericDAO = new GenericDAO<>(Employee.class);

            if(genericDAO.persist(employee)){
                clear();
                FunctionUtil.alert("Data successfully registered!");
            }else{
                FunctionUtil.alert("Error registering data!");
            }
        });

        btnClear.addActionListener(e -> {
            clear();
        });

        frame.getContentPane().add(jPanel);
        frame.setVisible(true);
    }

    private JLabel lblJourney;
    private JTextField textFieldJourney;

    private JLabel lblDescription;
    private JTextField textFieldDescription;

    private void hiddenFieldsCleaningAssistant(){
        jPanel.remove(lblJourney);
        jPanel.remove(textFieldJourney);

        jPanel.remove(lblDescription);
        jPanel.remove(textFieldDescription);
    }

    private void showFieldsCleaningAssistant(){
        lblName = new JLabel("Name (*)");
        jPanel.add(lblName);

        textFieldName = new JTextField();
        textFieldName.setColumns(10);
        jPanel.add(textFieldName);

        lblAddress = new JLabel("Address (*) ");
        jPanel.add(lblAddress);

        textFieldAddress = new JTextField();
        textFieldAddress.setColumns(10);
        jPanel.add(textFieldAddress);

        lblSex = new JLabel("Sex (*)");
        jPanel.add(lblSex);

        comboBoxSex = new JComboBox<String>();
        comboBoxSex.addItem("Select an option");
        comboBoxSex.addItem("Male");
        comboBoxSex.addItem("Female");
        jPanel.add(comboBoxSex);

        lblBirthday = new JLabel("Birthday (*)");
        jPanel.add(lblBirthday);

        textFieldBirthday = new JTextField();
        textFieldBirthday.setColumns(10);
        jPanel.add(textFieldBirthday);

        lblSalary = new JLabel("Salary (*) {1.0}");
        jPanel.add(lblSalary);

        textFieldSalary = new JTextField();
        textFieldSalary.setColumns(10);
        jPanel.add(textFieldSalary);

        lblActing = new JLabel("Office (*)");
        jPanel.add(lblActing);

        textFieldActing = new JTextField();
        textFieldActing.setColumns(10);
        jPanel.add(textFieldActing);

        lblJourney = new JLabel("Journey (*)");
        jPanel.add(lblJourney);

        textFieldJourney = new JTextField();
        jPanel.add(textFieldJourney);

        lblDescription = new JLabel("Description");
        jPanel.add(lblDescription);

        textFieldDescription = new JTextField();
        textFieldDescription.setColumns(10);
        jPanel.add(textFieldDescription);

        btnClear = new JButton("Clear");
        jPanel.add(btnClear);

        btnSubmit = new JButton("Submit");
        btnSubmit.setBackground(ConstantUtil.BLUE_APP_COLOR);
        btnSubmit.setForeground(ConstantUtil.GREEN_APP_COLOR);
        btnSubmit.setSize(100, 100);
        jPanel.add(btnSubmit);

        btnSubmit.addActionListener(e -> {
            CleaningAssistant cleaningAssistant = new CleaningAssistant(
                    textFieldName.getText(),
                    textFieldAddress.getText(),
                    Objects.requireNonNull(comboBoxSex.getSelectedItem()).toString(),
                    textFieldBirthday.getText(),
                    Double.parseDouble(textFieldSalary.getText()),
                    textFieldActing.getText(),
                    textFieldJourney.getText(),
                    textFieldDescription.getText()
            );

            Employee<CleaningAssistant> employee = new Employee<>();
            employee.setRole(roles.get(comboBoxTypeEmployee.getSelectedIndex()-1));
            employee.setDepartment(departments.get(comboBoxDepartment.getSelectedIndex()-1));
            employee.setData(cleaningAssistant);

            GenericDAO<Employee> genericDAO = new GenericDAO<>(Employee.class);

            if(genericDAO.persist(employee)){
                clear();
                FunctionUtil.alert("Data successfully registered!");
            }else{
                FunctionUtil.alert("Error registering data!");
            }
        });

        btnClear.addActionListener(e -> {
            clear();
        });

        frame.getContentPane().add(jPanel);
        frame.setVisible(true);
    }
}