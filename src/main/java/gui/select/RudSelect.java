package gui.select;

import gui.form.DepartmentForm;
import gui.form.EmployeeForm;
import gui.show.DepartmentShow;
import gui.show.DependentShow;
import gui.show.EmployeeShow;
import gui.show.ProjectShow;
import util.ConstantUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

public class RudSelect {
    public void showOptions(){
        JPanel jPanel = new JPanel();
        JFrame frame = new JFrame();

        JRadioButton jRadioButtonDepartment = new JRadioButton(ConstantUtil.OPTIONS[0]);
        JRadioButton jRadioButtonDependent = new JRadioButton(ConstantUtil.OPTIONS[1]);
        JRadioButton jRadioButtonEmployee = new JRadioButton(ConstantUtil.OPTIONS[2]);
        JRadioButton jRadioButtonProject = new JRadioButton(ConstantUtil.OPTIONS[3]);

        jRadioButtonDepartment.setMnemonic(KeyEvent.VK_A);
        jRadioButtonDependent.setMnemonic(KeyEvent.VK_B);
        jRadioButtonEmployee.setMnemonic(KeyEvent.VK_C);
        jRadioButtonProject.setMnemonic(KeyEvent.VK_D);

        jRadioButtonDepartment.addActionListener(e -> {
                new DepartmentShow().show();
        });

        jRadioButtonDependent.addActionListener(e -> {
                new DependentShow().show();
        });

        jRadioButtonEmployee.addActionListener(e -> {
                new EmployeeShow().show();
        });

        jRadioButtonProject.addActionListener(e -> {
                new ProjectShow().show();
        });

        ButtonGroup group = new ButtonGroup();
        group.add(jRadioButtonDepartment);
        group.add(jRadioButtonDependent);
        group.add(jRadioButtonEmployee);
        group.add(jRadioButtonProject);

        jPanel.setLayout(new GridLayout(3, 1));
        jPanel.add(jRadioButtonDepartment);
        jPanel.add(jRadioButtonDependent);
        jPanel.add(jRadioButtonEmployee);
        jPanel.add(jRadioButtonProject);

        jPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "Select the option you wish to view"));

        frame.getContentPane().add(jPanel);

        frame.setSize(new Dimension(400, 400));
        frame.setResizable(false);
        frame.setMinimumSize(new Dimension(400, 400));
        frame.pack();

        frame.setLocationRelativeTo(null);

        try {
            // 1.6+
            frame.setLocationByPlatform(true);
            frame.setMinimumSize(frame.getSize());
        } catch(Throwable ignoreAndContinue) {
            ignoreAndContinue.printStackTrace();
        }

        frame.setVisible(true);
    }
}
