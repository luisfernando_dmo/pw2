package gui.show;

import dao.GenericDAO;
import entity.Department;
import util.ConstantUtil;
import util.FunctionUtil;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class DepartmentShow {

    private List<Department> departments;

    public DepartmentShow(){
        this.departments = new GenericDAO<Department>(Department.class).findAll();
    }

    private String[] tags = {"id", "name", "number", "created_at", "updated_at", "", ""};

    public void show(){
        JFrame frame = new JFrame("RUD - Department");

        JPanel panel;
        JTable table;
        JScrollPane scrollPane;

        panel = new JPanel();

        DefaultTableModel tableModel = setDefaultTableModel(tags, true);

        for (Department department : departments) {
            Object[] data = {
                    department.getId(),
                    department.getName(),
                    department.getNumber(),
                    department.getCreatedAt(),
                    department.getUpdatedAt()
            };

            tableModel.addRow(data);
        }

        panel.setLayout(new GridLayout(1, 1));
        table = new JTable(tableModel);

        scrollPane = new JScrollPane(table);
        panel.add(scrollPane);

        frame.getContentPane().add(panel);

        new ButtonColumn(departments,"UPDATE", table, tags.length - 2);
        new ButtonColumn(departments,"DELETE", table, tags.length - 1);

        frame.setSize(new Dimension(1000, 400));
        frame.setResizable(false);
        frame.setMinimumSize(new Dimension(1000, 400));

        frame.setLocationRelativeTo(null);

        try {
            // 1.6+
            frame.setLocationByPlatform(true);
            frame.setMinimumSize(frame.getSize());
        } catch(Throwable ignoreAndContinue) {
            ignoreAndContinue.printStackTrace();
        }

        frame.setVisible(true);
    }

    private DefaultTableModel setDefaultTableModel(String[] column, boolean isEditable){
        return new DefaultTableModel(column, 0){
            @Override
            public boolean isCellEditable(int row, int column) {
                if(column == 0 || column == 3 || column == 4){
                    return false;
                }
                return isEditable;
            }
        };
    }

    static class ButtonColumn extends AbstractCellEditor
            implements TableCellRenderer, TableCellEditor, ActionListener {
        JTable table;
        JButton renderButton;
        JButton editButton;
        String text;
        String name;
        List<Department> departments;

        ButtonColumn(List<Department> departments, String name, JTable table, int column) {
            super();
            this.table = table;
            this.name = name;
            this.departments = departments;
            renderButton = new JButton();

            editButton = new JButton();
            editButton.setFocusPainted(false);
            editButton.addActionListener(this);

            TableColumnModel columnModel = table.getColumnModel();
            columnModel.getColumn(column).setCellRenderer(this);
            columnModel.getColumn(column).setCellEditor(this);
        }

        public Component getTableCellRendererComponent(
                JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if (hasFocus) {
                if(name.toLowerCase().equals("update")){
                    renderButton.setForeground(ConstantUtil.GREEN_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.GREEN_APP_COLOR);
                }else{
                    renderButton.setForeground(ConstantUtil.RED_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.RED_APP_COLOR);
                }
            } else if (isSelected) {
                if(name.toLowerCase().equals("update")){
                    renderButton.setForeground(ConstantUtil.GREEN_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.GREEN_APP_COLOR);
                }else{
                    renderButton.setForeground(ConstantUtil.RED_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.RED_APP_COLOR);
                }
            } else {
                if(name.toLowerCase().equals("update")){
                    renderButton.setForeground(ConstantUtil.GREEN_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.GREEN_APP_COLOR);
                }else{
                    renderButton.setForeground(ConstantUtil.RED_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.RED_APP_COLOR);
                }
            }

            renderButton.setText(name);
            return renderButton;
        }

        public Component getTableCellEditorComponent(
                JTable table, Object value, boolean isSelected, int row, int column) {
            text = (value == null) ? "" : value.toString();
            editButton.setText(text);
            return editButton;
        }

        public Object getCellEditorValue() {
            return text;
        }

        public void actionPerformed(ActionEvent e) {
            fireEditingStopped();

            TableModel model = table.getModel();

            Department department = new Department();
            department.setId(departments.get(table.getSelectedRow()).getId());
            department.setProjects(departments.get(table.getSelectedRow()).getProjects());

            GenericDAO<Department> departmentGenericDAO = new GenericDAO<>(Department.class);

            if(name.toLowerCase().equals("update")){

                department.setName(model.getValueAt(table.getSelectedRow(), 1).toString());
                department.setNumber(Integer.parseInt(
                        model.getValueAt(table.getSelectedRow(), 2).toString()
                ));

                departmentGenericDAO.update(department);
            }else{
                if(departmentGenericDAO.delete(department)){
                    ((DefaultTableModel)model).removeRow(table.getSelectedRow());
                }else{
                    FunctionUtil.alert("Error deleting department!");
                }
            }
        }
    }
}