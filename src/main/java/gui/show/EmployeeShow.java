package gui.show;

import com.google.gson.Gson;
import dao.GenericDAO;
import entity.Department;
import entity.Employee;
import entity.Role;
import model.CleaningAssistant;
import model.Researcher;
import model.Secretary;
import org.json.JSONObject;
import util.ConstantUtil;
import util.FunctionUtil;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.util.Objects;

public class EmployeeShow {
    private List<Employee> employees;

    public EmployeeShow(){
        this.employees = new GenericDAO<Employee>(Employee.class).findAll();
    }

    private String[] tags = {"id", "sex (C)", "name (C)", "acting/office (R - CA)",  "salary (C)", "address (C)",  "birthday (C)", "journey (CA)", "schooling (S)", "description (CA)", "role_id (TID)", "role_name (T)", "created_at", "updated_at", "", ""};

    public void show(){
        JFrame frame = new JFrame("RUD - Employee");

        JPanel panel;
        JTable table;
        JScrollPane scrollPane;

        panel = new JPanel();

        DefaultTableModel tableModel = setDefaultTableModel(tags, true);

        for (Employee employee : employees) {
            String json = new Gson().toJson(employee.getData());
            JSONObject object = new JSONObject(json);

            Object[] data = null;

            switch (employee.getRole().getId()){
                case 1:
                    data = new Object[]{
                            employee.getId(),
                            object.getString("sex"),
                            object.getString("name"),
                            object.getString("acting"),
                            object.getDouble("salary"),
                            object.getString("address"),
                            object.getString("birthday"),
                            "-",
                            "-",
                            "-",
                            employee.getRole().getId(),
                            employee.getRole().getName(),
                            employee.getCreatedAt(),
                            employee.getUpdatedAt()
                    };
                    break;
                case 2:
                    data = new Object[]{
                            employee.getId(),
                            object.getString("sex"),
                            object.getString("name"),
                            "-",
                            object.getDouble("salary"),
                            object.getString("address"),
                            object.getString("birthday"),
                            "-",
                            object.getString("schooling"),
                            "-",
                            employee.getRole().getId(),
                            employee.getRole().getName(),
                            employee.getCreatedAt(),
                            employee.getUpdatedAt()
                    };
                    break;
                case 3:
                    data = new Object[]{
                            employee.getId(),
                            object.getString("sex"),
                            object.getString("name"),
                            object.getString("office"),
                            object.getDouble("salary"),
                            object.getString("address"),
                            object.getString("birthday"),
                            object.getString("journey"),
                            "-",
                            object.getString("description"),
                            employee.getRole().getId(),
                            employee.getRole().getName(),
                            employee.getCreatedAt(),
                            employee.getUpdatedAt()
                    };
                    break;
            }

            tableModel.addRow(data);
        }

        panel.setLayout(new GridLayout(1, 1));

        table = new JTable(tableModel);

        scrollPane = new JScrollPane(table);
        panel.add(scrollPane);

        panel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "<html>Subtitle<br>(C) Common | (R) Researcher | (S) Secretary | (CA) Cleaning Assistant | (T) Type</html>"));

        frame.getContentPane().add(panel);

        new ButtonColumn(employees,"UPDATE", table, tags.length - 2);
        new ButtonColumn(employees,"DELETE", table, tags.length - 1);

        frame.setSize(new Dimension(1400, 600));
        frame.setResizable(false);
        frame.setMinimumSize(new Dimension(1400, 600));

        frame.setLocationRelativeTo(null);

        try {
            // 1.6+
            frame.setLocationByPlatform(true);
            frame.setMinimumSize(frame.getSize());
        } catch(Throwable ignoreAndContinue) {
            ignoreAndContinue.printStackTrace();
        }

        frame.setVisible(true);
    }

    private DefaultTableModel setDefaultTableModel(String[] column, boolean isEditable){
        return new DefaultTableModel(column, 0){
            @Override
            public boolean isCellEditable(int row, int column) {
                if(column == 0 || column == 10 || column == 11 || column == 12 || column == 13){
                    return false;
                }
                return isEditable;
            }
        };
    }

    static class ButtonColumn extends AbstractCellEditor
            implements TableCellRenderer, TableCellEditor, ActionListener {
        JTable table;
        JButton renderButton;
        JButton editButton;
        String text;
        String name;
        List<Employee> employees;

        ButtonColumn(List<Employee> employees, String name, JTable table, int column) {
            super();
            this.table = table;
            this.name = name;
            this.employees = employees;
            renderButton = new JButton();

            editButton = new JButton();
            editButton.setFocusPainted(false);
            editButton.addActionListener(this);

            TableColumnModel columnModel = table.getColumnModel();
            columnModel.getColumn(column).setCellRenderer(this);
            columnModel.getColumn(column).setCellEditor(this);
        }

        public Component getTableCellRendererComponent(
                JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if (hasFocus) {
                if(name.toLowerCase().equals("update")){
                    renderButton.setForeground(ConstantUtil.GREEN_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.GREEN_APP_COLOR);
                }else{
                    renderButton.setForeground(ConstantUtil.RED_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.RED_APP_COLOR);
                }
            } else if (isSelected) {
                if(name.toLowerCase().equals("update")){
                    renderButton.setForeground(ConstantUtil.GREEN_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.GREEN_APP_COLOR);
                }else{
                    renderButton.setForeground(ConstantUtil.RED_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.RED_APP_COLOR);
                }
            } else {
                if(name.toLowerCase().equals("update")){
                    renderButton.setForeground(ConstantUtil.GREEN_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.GREEN_APP_COLOR);
                }else{
                    renderButton.setForeground(ConstantUtil.RED_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.RED_APP_COLOR);
                }
            }

            renderButton.setText(name);
            return renderButton;
        }

        public Component getTableCellEditorComponent(
                JTable table, Object value, boolean isSelected, int row, int column) {
            text = (value == null) ? "" : value.toString();
            editButton.setText(text);
            return editButton;
        }

        public Object getCellEditorValue() {
            return text;
        }

        public void actionPerformed(ActionEvent e) {
            fireEditingStopped();

            TableModel model = table.getModel();

            int role_id = Integer.parseInt(model.getValueAt(table.getSelectedRow(), 10).toString());
            int employee_id = Integer.parseInt(model.getValueAt(table.getSelectedRow(), 0).toString());

            Role role = new Role();
            role.setId(role_id);

            if(name.toLowerCase().equals("update")) {
                switch (role_id){
                    case 1:
                        Researcher researcher = new Researcher(
                                model.getValueAt(table.getSelectedRow(), 2).toString(),
                                model.getValueAt(table.getSelectedRow(), 5).toString(),
                                model.getValueAt(table.getSelectedRow(), 1).toString(),
                                model.getValueAt(table.getSelectedRow(), 6).toString(),
                                Double.parseDouble(model.getValueAt(table.getSelectedRow(), 4).toString()),
                                model.getValueAt(table.getSelectedRow(), 3).toString()
                        );

                        Employee<Researcher> employeeResearcher = new Employee<>();
                        employeeResearcher.setId(employee_id);
                        employeeResearcher.setRole(role);

                        employeeResearcher.setData(researcher);

                        GenericDAO<Employee> genericDAO = new GenericDAO<>(Employee.class);
                        genericDAO.update(employeeResearcher);
                        break;

                    case 2:

                        Secretary secretary = new Secretary(
                                model.getValueAt(table.getSelectedRow(), 2).toString(),
                                model.getValueAt(table.getSelectedRow(), 5).toString(),
                                model.getValueAt(table.getSelectedRow(), 1).toString(),
                                model.getValueAt(table.getSelectedRow(), 6).toString(),
                                Double.parseDouble(model.getValueAt(table.getSelectedRow(), 4).toString()),
                                model.getValueAt(table.getSelectedRow(), 8).toString()
                        );

                        Employee<Secretary> employeeSecretary = new Employee<>();
                        employeeSecretary.setId(employee_id);
                        employeeSecretary.setRole(role);

                        employeeSecretary.setData(secretary);

                        GenericDAO<Employee> genericDAOSecretary = new GenericDAO<>(Employee.class);
                        genericDAOSecretary.update(employeeSecretary);
                        break;


                    case 3:
                        CleaningAssistant cleaningAssistant = new CleaningAssistant(
                                model.getValueAt(table.getSelectedRow(), 2).toString(),
                                model.getValueAt(table.getSelectedRow(), 5).toString(),
                                model.getValueAt(table.getSelectedRow(), 1).toString(),
                                model.getValueAt(table.getSelectedRow(), 6).toString(),
                                Double.parseDouble(model.getValueAt(table.getSelectedRow(), 4).toString()),
                                model.getValueAt(table.getSelectedRow(), 3).toString(),
                                model.getValueAt(table.getSelectedRow(), 7).toString(),
                                model.getValueAt(table.getSelectedRow(), 9).toString()
                        );

                        Employee<CleaningAssistant> employeeCleaningAssistant = new Employee<>();
                        employeeCleaningAssistant.setId(employee_id);
                        employeeCleaningAssistant.setRole(role);

                        employeeCleaningAssistant.setData(cleaningAssistant);

                        GenericDAO<Employee> genericDAOCleaningAssistant = new GenericDAO<>(Employee.class);
                        genericDAOCleaningAssistant.update(employeeCleaningAssistant);
                        break;
                }
            }else{
                GenericDAO<Employee> genericDAO = new GenericDAO<>(Employee.class);

                if(genericDAO.delete(employees.get(table.getSelectedRow()))){
                    ((DefaultTableModel)model).removeRow(table.getSelectedRow());
                }else{
                    FunctionUtil.alert("Error deleting employee!");
                }
            }
        }
    }
}