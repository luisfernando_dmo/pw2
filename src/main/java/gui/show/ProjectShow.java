package gui.show;

import com.google.gson.Gson;
import dao.GenericDAO;
import entity.Department;
import entity.Project;
import org.json.JSONObject;
import util.ConstantUtil;
import util.FunctionUtil;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class ProjectShow {
    private List<Project> projects;

    public ProjectShow(){
        this.projects = new GenericDAO<Project>(Project.class).findAll();
    }

    private String[] tags = {"id", "name", "hour", "date", "department_id", "department_name", "employee_id", "employee_name", "created_at", "updated_at", "", ""};

    public void show(){
        JFrame frame = new JFrame("RUD - Project");

        JPanel panel;
        JTable table;
        JScrollPane scrollPane;

        panel = new JPanel();

        DefaultTableModel tableModel = setDefaultTableModel(tags, true);

        for (Project project : projects) {

            String json = new Gson().toJson(project.getEmployee().getData());
            JSONObject employee = new JSONObject(json);

            Object[] data = {
                    project.getId(),
                    project.getName(),
                    project.getHour(),
                    project.getDate(),
                    project.getDepartment() != null ? project.getDepartment().getId() : "NULL",
                    project.getDepartment() != null ? project.getDepartment().getName() : "NULL",
                    project.getEmployee().getId(),
                    employee.getString("name"),
                    project.getCreatedAt(),
                    project.getUpdatedAt()
            };

            tableModel.addRow(data);
        }

        panel.setLayout(new GridLayout(1, 1));
        table = new JTable(tableModel);

        scrollPane = new JScrollPane(table);
        panel.add(scrollPane);

        frame.getContentPane().add(panel);

        new ButtonColumn(projects,"UPDATE", table, tags.length - 2);
        new ButtonColumn(projects,"DELETE", table, tags.length - 1);

        frame.setSize(new Dimension(1400, 500));
        frame.setResizable(false);
        frame.setMinimumSize(new Dimension(1400, 400));

        frame.setLocationRelativeTo(null);

        try {
            // 1.6+
            frame.setLocationByPlatform(true);
            frame.setMinimumSize(frame.getSize());
        } catch(Throwable ignoreAndContinue) {
            ignoreAndContinue.printStackTrace();
        }

        frame.setVisible(true);
    }

    private DefaultTableModel setDefaultTableModel(String[] column, boolean isEditable){
        return new DefaultTableModel(column, 0){
            @Override
            public boolean isCellEditable(int row, int column) {
                if(column == 0 || column == 4 || column == 5 || column == 6 || column == 7 || column == 8 || column == 9 ){
                    return false;
                }
                return isEditable;
            }
        };
    }

    static class ButtonColumn extends AbstractCellEditor
            implements TableCellRenderer, TableCellEditor, ActionListener {
        JTable table;
        JButton renderButton;
        JButton editButton;
        String text;
        String name;
        List<Project> projects;

        ButtonColumn(List<Project> projects, String name, JTable table, int column) {
            super();
            this.table = table;
            this.name = name;
            this.projects = projects;
            renderButton = new JButton();

            editButton = new JButton();
            editButton.setFocusPainted(false);
            editButton.addActionListener(this);

            TableColumnModel columnModel = table.getColumnModel();
            columnModel.getColumn(column).setCellRenderer(this);
            columnModel.getColumn(column).setCellEditor(this);
        }

        public Component getTableCellRendererComponent(
                JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if (hasFocus) {
                if(name.toLowerCase().equals("update")){
                    renderButton.setForeground(ConstantUtil.GREEN_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.GREEN_APP_COLOR);
                }else{
                    renderButton.setForeground(ConstantUtil.RED_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.RED_APP_COLOR);
                }
            } else if (isSelected) {
                if(name.toLowerCase().equals("update")){
                    renderButton.setForeground(ConstantUtil.GREEN_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.GREEN_APP_COLOR);
                }else{
                    renderButton.setForeground(ConstantUtil.RED_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.RED_APP_COLOR);
                }
            } else {
                if(name.toLowerCase().equals("update")){
                    renderButton.setForeground(ConstantUtil.GREEN_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.GREEN_APP_COLOR);
                }else{
                    renderButton.setForeground(ConstantUtil.RED_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.RED_APP_COLOR);
                }
            }

            renderButton.setText(name);
            return renderButton;
        }

        public Component getTableCellEditorComponent(
                JTable table, Object value, boolean isSelected, int row, int column) {
            text = (value == null) ? "" : value.toString();
            editButton.setText(text);
            return editButton;
        }

        public Object getCellEditorValue() {
            return text;
        }

        public void actionPerformed(ActionEvent e) {
            fireEditingStopped();

            TableModel model = table.getModel();

            Project project = new Project();
            project.setId(Integer.parseInt(
                    model.getValueAt(table.getSelectedRow(), 0).toString()));
            project.setDepartment(projects.get(table.getSelectedRow()).getDepartment());
            project.setEmployee(projects.get(table.getSelectedRow()).getEmployee());

            GenericDAO<Project> projectGenericDAO = new GenericDAO<>(Project.class);

            if (name.toLowerCase().equals("update")) {
                project.setName(model.getValueAt(table.getSelectedRow(), 1).toString());
                project.setHour(model.getValueAt(table.getSelectedRow(), 2).toString());
                project.setDate(model.getValueAt(table.getSelectedRow(), 3).toString());

                projectGenericDAO.update(project);
            } else {
                if (projectGenericDAO.delete(project)) {
                    ((DefaultTableModel) model).removeRow(table.getSelectedRow());
                } else {
                    FunctionUtil.alert("Error deleting project!");
                }
            }

        }
    }
}