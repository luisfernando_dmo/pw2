package gui.show;

import com.google.gson.Gson;
import dao.GenericDAO;
import entity.Department;
import entity.Dependent;
import entity.Project;
import org.json.JSONObject;
import util.ConstantUtil;
import util.FunctionUtil;

import javax.swing.*;
import javax.swing.table.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class DependentShow {
    private List<Dependent> dependents;

    public DependentShow(){
        this.dependents = new GenericDAO<Dependent>(Dependent.class).findAll();
    }

    private String[] tags = {"id", "name", "sex", "birthday", "kinship", "employee_id", "employee_name", "created_at", "updated_at", "", ""};

    public void show(){
        JFrame frame = new JFrame("RUD - Dependent");

        JPanel panel;
        JTable table;
        JScrollPane scrollPane;

        panel = new JPanel();

        DefaultTableModel tableModel = setDefaultTableModel(tags, true);

        for (Dependent dependent : dependents) {
            String json = new Gson().toJson(dependent.getEmployee().getData());
            JSONObject object = new JSONObject(json);

            Object[] data = {
                    dependent.getId(),
                    dependent.getName(),
                    dependent.getSex(),
                    dependent.getBirthday(),
                    dependent.getKinship(),
                    dependent.getEmployee().getId(),
                    object.getString("name"),
                    dependent.getCreatedAt(),
                    dependent.getUpdatedAt()
            };

            tableModel.addRow(data);
        }

        panel.setLayout(new GridLayout(1, 1));
        table = new JTable(tableModel);

        scrollPane = new JScrollPane(table);
        panel.add(scrollPane);

        frame.getContentPane().add(panel);

        new ButtonColumn(dependents,"UPDATE", table, tags.length - 2);
        new ButtonColumn(dependents,"DELETE", table, tags.length - 1);

        frame.setSize(new Dimension(1200, 420));
        frame.setResizable(false);
        frame.setMinimumSize(new Dimension(1200, 420));

        frame.setLocationRelativeTo(null);

        try {
            // 1.6+
            frame.setLocationByPlatform(true);
            frame.setMinimumSize(frame.getSize());
        } catch(Throwable ignoreAndContinue) {
            ignoreAndContinue.printStackTrace();
        }

        frame.setVisible(true);
    }

    private DefaultTableModel setDefaultTableModel(String[] column, boolean isEditable){
        return new DefaultTableModel(column, 0){
            @Override
            public boolean isCellEditable(int row, int column) {
                if(column == 0 || column == 5 || column == 6 || column == 7 || column == 8){
                    return false;
                }
                return isEditable;
            }
        };
    }

    static class ButtonColumn extends AbstractCellEditor
            implements TableCellRenderer, TableCellEditor, ActionListener {
        JTable table;
        JButton renderButton;
        JButton editButton;
        String text;
        String name;
        List<Dependent> dependents;

        ButtonColumn(List<Dependent> dependents, String name, JTable table, int column) {
            super();
            this.table = table;
            this.name = name;
            this.dependents = dependents;
            renderButton = new JButton();

            editButton = new JButton();
            editButton.setFocusPainted(false);
            editButton.addActionListener(this);

            TableColumnModel columnModel = table.getColumnModel();
            columnModel.getColumn(column).setCellRenderer(this);
            columnModel.getColumn(column).setCellEditor(this);
        }

        public Component getTableCellRendererComponent(
                JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            if (hasFocus) {
                if(name.toLowerCase().equals("update")){
                    renderButton.setForeground(ConstantUtil.GREEN_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.GREEN_APP_COLOR);
                }else{
                    renderButton.setForeground(ConstantUtil.RED_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.RED_APP_COLOR);
                }
            } else if (isSelected) {
                if(name.toLowerCase().equals("update")){
                    renderButton.setForeground(ConstantUtil.GREEN_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.GREEN_APP_COLOR);
                }else{
                    renderButton.setForeground(ConstantUtil.RED_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.RED_APP_COLOR);
                }
            } else {
                if(name.toLowerCase().equals("update")){
                    renderButton.setForeground(ConstantUtil.GREEN_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.GREEN_APP_COLOR);
                }else{
                    renderButton.setForeground(ConstantUtil.RED_APP_COLOR);
                    renderButton.setBackground(ConstantUtil.RED_APP_COLOR);
                }
            }

            renderButton.setText(name);
            return renderButton;
        }

        public Component getTableCellEditorComponent(
                JTable table, Object value, boolean isSelected, int row, int column) {
            text = (value == null) ? "" : value.toString();
            editButton.setText(text);
            return editButton;
        }

        public Object getCellEditorValue() {
            return text;
        }

        public void actionPerformed(ActionEvent e) {
            fireEditingStopped();

            TableModel model = table.getModel();

            Dependent dependent = new Dependent();
            dependent.setId(Integer.parseInt(
                    model.getValueAt(table.getSelectedRow(), 0).toString()));
            dependent.setEmployee(dependents.get(table.getSelectedRow()).getEmployee());

            GenericDAO<Dependent> dependentGenericDAO = new GenericDAO<>(Dependent.class);

            if(name.toLowerCase().equals("update")){
                dependent.setName(model.getValueAt(table.getSelectedRow(), 1).toString());
                dependent.setSex(model.getValueAt(table.getSelectedRow(), 2).toString());
                dependent.setBirthday(model.getValueAt(table.getSelectedRow(), 3).toString());
                dependent.setKinship(model.getValueAt(table.getSelectedRow(), 4).toString());

                dependentGenericDAO.update(dependent);
            }else{
                if(dependentGenericDAO.delete(dependent)){
                    ((DefaultTableModel)model).removeRow(table.getSelectedRow());
                }else{
                    FunctionUtil.alert("Error deleting department!");
                }
            }
        }
    }
}