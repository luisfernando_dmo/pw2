package entity;

import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "employees")
public class Employee<T> extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "role_id")
    private Role role;

    @OneToMany(mappedBy = "id")
    private Set<Project> projects;

    @OneToMany(mappedBy = "id")
    private Set<Dependent> dependent;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    private Department department;

    @Type(type = "json")
    @Column(columnDefinition = "json")
    private T data;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Set<Dependent> getDependent() {
        return dependent;
    }

    public void setDependent(Set<Dependent> dependent) {
        this.dependent = dependent;
    }
}