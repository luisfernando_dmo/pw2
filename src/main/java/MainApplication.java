import gui.action.MenuAction;
import seed.RoleSeeder;
import util.HibernateUtil;
import gui.Menu;

import java.awt.*;

public class MainApplication {
    public static void main(String[] args) {
        HibernateUtil.getInstance();
        RoleSeeder.execute(HibernateUtil.getEntityManager());

        EventQueue.invokeLater(new Runnable() {
            public void run() {
                initialize();
            }
        });
    }

    private static void initialize(){
        try {
            String projectName = "PW2 - Luís Fernando";
            String[] buttonsName = {
                    "CREATE",
                    "RUD (INTEGRATED)"
            };

            Dimension sizeScreen = new Dimension(400, 400);

            new Menu(
                    projectName,
                    buttonsName,
                    new MenuAction().getActions(),
                    sizeScreen
            ).execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}