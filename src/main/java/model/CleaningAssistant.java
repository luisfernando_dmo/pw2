package model;

import java.io.Serializable;
import java.util.Date;

public class CleaningAssistant implements Serializable {
    private String name;
    private String address;
    private String sex;
    private String birthday;
    private Double salary;
    private String office;
    private String journey;
    private String description;

    public CleaningAssistant(){}

    public CleaningAssistant(String name, String address, String sex, String birthday, Double salary, String office, String journey, String description) {
        this.name = name;
        this.address = address;
        this.sex = sex;
        this.birthday = birthday;
        this.salary = salary;
        this.office = office;
        this.journey = journey;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getJourney() {
        return journey;
    }

    public void setJourney(String journey) {
        this.journey = journey;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}